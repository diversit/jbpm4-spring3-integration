<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>eu.diversit</groupId>
	<name>DiversIT Main Pom</name>
	<version>1.2</version>
	<artifactId>diversit</artifactId>
	<packaging>pom</packaging>
	<description>Main pom for all DiversIT projects</description>
	<inceptionYear>2009</inceptionYear>
	<url>http://maven.diversit.eu/maven/docs/</url>
	<licenses>
		<license>
			<name>Apache 2</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
			<comments>A business-friendly OSS license for most (or all) open source software from DiversIT Europe</comments>
		</license>
	</licenses>
	<properties>
		<projectKey>DIVPOM</projectKey>
		<projectScmPath>diversit_repo/eu/diversit/diversit</projectScmPath>
	</properties>
	<issueManagement>
		<system>Jira</system>
		<url>http://jira:8080/browse/${projectKey}</url>
	</issueManagement>
	<ciManagement>
		<system>Bamboo</system>
		<url>http://bamboo:8085/browse/${projectKey}</url>
	</ciManagement>
	<scm>
		<!--
			connection>scm:svn:http://svn/${projectScmPath}/trunk</connection -->
		<developerConnection>scm:svn:svn+ssh://svn.diversit.eu/home/svn/diversit_repo/eu/diversit/diversit/tags/diversit-1.2</developerConnection>
		<url>http://fisheye:8060/browse/diversit_repo/eu/diversit/diversit/tags/diversit-1.2</url>
	</scm>
	<developers>
		<developer>
			<id>jdboer</id>
			<name>Joost den Boer</name>
			<email>jdboer@diversit.eu</email>
			<url>http://joost.diversit.eu</url>
			<timezone>+1</timezone>
		</developer>
	</developers>
	<organization>
		<name>DiversIT Europe</name>
		<url>www.diversit.eu</url>
	</organization>
	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>2.0.2</version>
					<configuration>
						<source>1.5</source>
						<target>1.5</target>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-surefire-plugin</artifactId>
					<version>2.4.3</version>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
	<profiles>
		<profile>
			<id>dev-local</id>
			<distributionManagement>
				<repository>
					<id>local-releases</id>
					<name>diversit-releases</name>
					<url>http://artifactory:8082/artifactory/diversit-releases-local</url>
				</repository>
				<snapshotRepository>
					<id>local-snapshots</id>
					<name>diversit-snapshots</name>
					<url>http://artifactory:8082/artifactory/diversit-snapshots-local</url>
				</snapshotRepository>
			</distributionManagement>
		</profile>
		<profile>
			<!-- Additional plugins for releasing code -->
			<id>release</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-source-plugin</artifactId>
						<version>2.1.1</version>
						<executions>
							<execution>
								<id>attach-sources</id>
								<phase>verify</phase>
								<goals>
									<goal>jar-no-fork</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
	
	<repositories>
		<repository>
			<id>diversit_releases</id>
			<name>DiversIT OpenSource Releases</name>
			<url>http://maven.diversit.eu/repository</url>
		</repository>
		<repository>
			<id>diversit_snapshots</id>
			<name>DiversIT OpenSource Snapshots</name>
			<url>http://maven.diversit.eu/snapshot</url>
		</repository>
	</repositories>
	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-project-info-reports-plugin</artifactId>
				<version>2.1</version>
			</plugin>
			<!--
				Disabled because does not work without a scm.connection property.
				<plugin> <groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-changelog-plugin</artifactId>
				<version>2.1</version> </plugin>
			-->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-docck-plugin</artifactId>
				<version>1.0</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.5</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jxr-plugin</artifactId>
				<version>2.1</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-pmd-plugin</artifactId>
				<version>2.4</version>
				<inherited>true</inherited>
				<configuration>
					<targetJdk>1.5</targetJdk>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-report-plugin</artifactId>
				<version>2.4.3</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>2.2</version>
			</plugin>
		</plugins>
	</reporting>
	<distributionManagement>
		<repository>
			<id>diversit</id>
			<name>DiversIT Releases</name>
			<url>scp://maven.diversit.eu/www/diversit/maven/repository</url>
		</repository>
		<snapshotRepository>
			<id>diversit</id>
			<name>DiversIT Snapshots</name>
			<url>scp://maven.diversit.eu/www/diversit/maven/snapshot</url>
		</snapshotRepository>
		<site>
			<id>diversit</id>
			<name>DiversIT Site Documentation</name>
			<url>scp://maven.diversit.eu/www/diversit/maven/docs</url>
		</site>
	</distributionManagement>
</project>
