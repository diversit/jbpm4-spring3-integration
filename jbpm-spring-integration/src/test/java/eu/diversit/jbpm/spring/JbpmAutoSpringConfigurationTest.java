package eu.diversit.jbpm.spring;

import org.jbpm.api.ExecutionService;
import org.jbpm.api.HistoryService;
import org.jbpm.api.IdentityService;
import org.jbpm.api.ManagementService;
import org.jbpm.api.ProcessEngine;
import org.jbpm.api.RepositoryService;
import org.jbpm.api.TaskService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Testing the {@link ProcessEngineResourcesFactoryBean}
 * 
 * @author joostdenboer
 *
 */
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationContext.xml", "/hsqldbEmbeddedDataSourceContext.xml" })
public class JbpmAutoSpringConfigurationTest {

	@Autowired
	ApplicationContext applicationContext;
	
	@Autowired
	ProcessEngine processEngine;
	
	@Autowired
	private ExecutionService executionService;
	
	@Autowired
	private HistoryService historyService;
	
	@Autowired
	private IdentityService identityService;
	
	@Autowired
	private ManagementService managementService;
	
	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private TaskService taskService;

	/**
	 * Test all auto configured Jbpm components are available in Spring context.
	 */
	@Test
	public void testJbpmConfiguration() {
		Assert.assertNotNull("No application context", applicationContext);
		
		Assert.assertNotNull("No process engine component", processEngine);
		testSingletonComponent(processEngine);

		Assert.assertNotNull("No execution service", executionService);
		testSingletonComponent(executionService);
		
		Assert.assertNotNull("No history service", historyService);
		testSingletonComponent(historyService);
		
		Assert.assertNotNull("No identity service", identityService);
		testSingletonComponent(identityService);
		
		Assert.assertNotNull("No management service", managementService);
		testSingletonComponent(managementService);
		
		Assert.assertNotNull("No repository service", repositoryService);
		testSingletonComponent(repositoryService);
		
		Assert.assertNotNull("No task service", taskService);
		testSingletonComponent(taskService);
	}
	
	/**
	 * Check if component is a singleton by checking if the autowired component
	 * and one of the same type looked up in the context are the same instances.
	 * 
	 * @param comp1
	 * @param componentName
	 */
	private void testSingletonComponent(Object comp1) {
		
		Class<?> compClass = comp1.getClass();
		Object comp2 = applicationContext.getBean(compClass);
		
		Assert.assertTrue("Singleton components are not the same instances!", comp1 == comp2);
	}
}
