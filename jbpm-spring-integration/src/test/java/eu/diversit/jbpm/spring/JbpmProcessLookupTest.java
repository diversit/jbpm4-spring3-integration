/**
 * 
 */
package eu.diversit.jbpm.spring;

import java.util.List;

import org.jbpm.api.ProcessDefinition;
import org.jbpm.api.RepositoryService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Testing the {@link JbpmProcessLookup} class
 * 
 * @author joostdenboer
 *
 */
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationContext.xml", "/hsqldbEmbeddedDataSourceContext.xml", "/testApplicationContext.xml"})
@Transactional
public class JbpmProcessLookupTest {

	@Autowired
	private RepositoryService repositoryService;
	
	private boolean setupCompleted = false;
	
	private String resourceName = "org/jbpm/examples/java/process_v2.jpdl.xml";
	
	@Autowired
	private JbpmProcessLookup lookup;
	
	/**
	 * Deploy additional process with same key
	 */
	@Before
	public void setup() {
		if(!setupCompleted) {
			// deploy newer version of same process
			String deploymentId = repositoryService.createDeployment()
				.addResourceFromClasspath(resourceName)
				.deploy();
			
			// test setup complete
			setupCompleted = true;
		}
	}
	
	@Test
	public void testLookupCurrent() {
		
		ProcessDefinition pd = lookup.getCurrent();
		Assert.assertNotNull("No process definition found", pd);
		
		Assert.assertEquals("Process key", "JavaKey", pd.getKey());
		Assert.assertTrue("Process version", pd.getVersion() > 1);
	}
	
	/**
	 * Test looking up all versions
	 */
	@Test
	public void testLookupAll() {
		
		List<ProcessDefinition> pds = lookup.getAllVersions();
		Assert.assertNotNull("No process definitions found", pds);
		Assert.assertEquals("pd's found", 2, pds.size());
	}

	/**
	 * Test lookup up a version
	 */
	@Test
	public void testLookupVersion() {
		
		ProcessDefinition pd2 = lookup.getVersion(2);
		Assert.assertNotNull("No process definition (2) found", pd2);
		Assert.assertEquals("Process key", "JavaKey", pd2.getKey());
		Assert.assertEquals("Process version", 2, pd2.getVersion());
		
		ProcessDefinition pd1 = lookup.getVersion(1);
		Assert.assertNotNull("No process definition (1) found", pd1);
		Assert.assertEquals("Process key", "JavaKey", pd1.getKey());
		Assert.assertEquals("Process version", 1, pd1.getVersion());
	}
}
