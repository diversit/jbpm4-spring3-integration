/**
 * 
 */
package eu.diversit.jbpm.spring;

import org.jbpm.api.ExecutionService;
import org.jbpm.api.HistoryService;
import org.jbpm.api.IdentityService;
import org.jbpm.api.ManagementService;
import org.jbpm.api.ProcessEngine;
import org.jbpm.api.RepositoryService;
import org.jbpm.api.TaskService;
import org.jbpm.pvm.internal.cfg.ConfigurationImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import eu.diversit.jbpm.command.GetEnvironmentObject;

/**
 * Factory bean which create the JBPM Process Engine.
 * 
 * @author joostdenboer
 * 
 */
@Configuration
public class JbpmAutoSpringConfiguration {

	protected String jbpmCfg = "jbpm.cfg.xml";

	public void setJbpmCfg(String jbpmCfg) {
		this.jbpmCfg = jbpmCfg;
	}

	@Autowired
	private ApplicationContext applicationContext;

	@Bean(name="jbpmProcessEngine")
	@Scope(BeanDefinition.SCOPE_SINGLETON)
	public ProcessEngine processEngine() {
		return new ConfigurationImpl().springInitiated(applicationContext)
				.setResource(jbpmCfg).buildProcessEngine();
	}

	/**
	 * Provides the {@link ExecutionService}. Qualifier name is
	 * 'jbpmExecutionService'.
	 * 
	 * @return {@link ExecutionService}
	 */
	@Bean(name = "jbpmExecutionService")
	public ExecutionService executionService() {
		return (ExecutionService) processEngine()
				.execute(new GetEnvironmentObject(ExecutionService.class));
	}

	/**
	 * Provides the {@link HistoryService}. Qualifier name is
	 * 'jbpmHistoryService'.
	 * 
	 * @return {@link HistoryService}
	 */
	@Bean
	@Qualifier("jbpmHistoryService")
	public HistoryService historyService() {
		return (HistoryService) processEngine().execute(new GetEnvironmentObject(
				HistoryService.class));
	}

	/**
	 * Provides the {@link IdentityService}. Qualifier name is
	 * 'jbpmIdentityService'.
	 * 
	 * @return {@link IdentityService}
	 */
	@Bean
	@Qualifier("jbpmIdentityService")
	public IdentityService identityService() {
		return (IdentityService) processEngine()
				.execute(new GetEnvironmentObject(IdentityService.class));
	}

	/**
	 * Provides the {@link ManagementService}. Qualifier name is
	 * 'jbpmManagementService'.
	 * 
	 * @return {@link ManagementService}
	 */
	@Bean
	@Qualifier("jbpmManagementService")
	public ManagementService managementService() {
		return (ManagementService) processEngine()
				.execute(new GetEnvironmentObject(ManagementService.class));
	}

	/**
	 * Provides the {@link RepositoryService}. Qualifier name is
	 * 'jbpmRepositoryService'.
	 * 
	 * @return {@link RepositoryService}
	 */
	@Bean
	@Qualifier("jbpmRepositoryService")
	public RepositoryService repositoryService() {
		return (RepositoryService) processEngine()
				.execute(new GetEnvironmentObject(RepositoryService.class));
	}

	/**
	 * Provides the {@link TaskService}. Qualifier name is 'jbpmTaskService'.
	 * 
	 * @return {@link TaskService}
	 */
	@Bean
	@Qualifier("jbpmTaskService")
	public TaskService taskService() {
		return (TaskService) processEngine().execute(new GetEnvironmentObject(
				TaskService.class));
	}

}
