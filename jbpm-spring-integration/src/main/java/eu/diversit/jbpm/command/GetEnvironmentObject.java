/**
 * 
 */
package eu.diversit.jbpm.command;

import org.jbpm.api.cmd.Command;
import org.jbpm.api.cmd.Environment;

/**
 * Get an object from the environment
 * 
 * @author joostdenboer
 *
 */
public class GetEnvironmentObject implements Command<Object> {

	private Class<?> objClass;
	
	public <T> GetEnvironmentObject(Class<?> objClazz) {
		this.objClass = objClazz;
	}
	
	private static final long serialVersionUID = 1L;
	
	public Object execute(Environment environment) throws Exception {
		Object es = environment.get(objClass);
		return es;
	}

}
