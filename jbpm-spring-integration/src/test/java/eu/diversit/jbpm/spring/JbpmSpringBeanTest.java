/**
 * 
 */
package eu.diversit.jbpm.spring;

import junit.framework.Assert;

import org.jbpm.api.ProcessInstance;
import org.jbpm.examples.java.Hand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author joostdenboer
 *
 */
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationContext.xml", "/hsqldbEmbeddedDataSourceContext.xml", "/testApplicationContext.xml"})
@Transactional
public class JbpmSpringBeanTest {

	@Autowired
	JbpmProcessBean testBean;
	
	@Test
	public void testStartProcessUsingKey() {
		// set key to use
		testBean.setProcessKey("JavaKey");
		
		Assert.assertNotNull("No JbpmProcessBean in context", testBean);
		Assert.assertNotNull("Process not deployed", testBean.getDeploymentId());
		
		ProcessInstance pi = testBean.startProcessInstance();
		
		Assert.assertNotNull("No ProcessDefinition", pi);
		System.out.println("Process state is: "+pi.getState());

		String answer = (String) testBean.getVariable(pi.getId(),"answer");
	    Assert.assertEquals("I'm fine, thank you.", answer);

	    Hand hand = (Hand) testBean.getVariable(pi.getId(), "hand");
	    Assert.assertTrue(hand.isShaken());
	    Assert.assertEquals(1, hand.getTimesShaken());
	    System.out.println("Times shaken = "+hand.getTimesShaken());
	}

	@Test
	public void testStartProcessWithoutUsingKey() {
		
		Assert.assertNotNull("No JbpmProcessBean in context", testBean);
		Assert.assertNotNull("Process not deployed", testBean.getDeploymentId());
		
		ProcessInstance pi = testBean.startProcessInstance();
		
		Assert.assertNotNull("No ProcessDefinition", pi);
		System.out.println("Process state is: "+pi.getState());
	
		String answer = (String) testBean.getVariable(pi.getId(),"answer");
	    Assert.assertEquals("I'm fine, thank you.", answer);
	
	    Hand hand = (Hand) testBean.getVariable(pi.getId(), "hand");
	    Assert.assertTrue(hand.isShaken());
	    Assert.assertEquals(1, hand.getTimesShaken());
	    System.out.println("Times shaken = "+hand.getTimesShaken());
	}
}
