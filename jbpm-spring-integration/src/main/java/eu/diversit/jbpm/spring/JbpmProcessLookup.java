/**
 * 
 */
package eu.diversit.jbpm.spring;

import java.util.List;

import org.jbpm.api.ProcessDefinition;
import org.jbpm.api.ProcessDefinitionQuery;
import org.jbpm.api.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Bean to lookup a ProcessDefinition by key.
 * Can be used to lookup the current (last) deployed process definition version.
 * 
 * @author joostdenboer
 */
public class JbpmProcessLookup {

	@Autowired
	private RepositoryService repositoryService;
	
	private String processKey;

	/**
	 * Get the current (last) version of the process with current process key.
	 * @return {@link ProcessDefinition}
	 */
	public ProcessDefinition getCurrent() {
		ProcessDefinitionQuery dfq = repositoryService.createProcessDefinitionQuery();
		List<ProcessDefinition> result = dfq.processDefinitionKey(processKey)
			.orderDesc(ProcessDefinitionQuery.PROPERTY_VERSION)
			.page(0, 1)
			.list();
		
		if(!result.isEmpty()) {
			return result.get(0);
		} else {
			return null;
		}
	}
	
	/**
	 * Get all versions of the process with current process key.
	 * @return List of {@link ProcessDefinition}
	 */
	public List<ProcessDefinition> getAllVersions() {
		
		List<ProcessDefinition> result = repositoryService.createProcessDefinitionQuery()
				.processDefinitionKey(processKey)
				.list();
		return result;
	}
	
	/**
	 * Get a process definition with given version.
	 * @param version int Specific version
	 * @return {@link ProcessDefinition} with current process key and given version.
	 */
	public ProcessDefinition getVersion(int version) {
		String id = String.format("%s-%d", processKey, version);
		
		ProcessDefinitionQuery dfq = repositoryService.createProcessDefinitionQuery();
		ProcessDefinition df = dfq.processDefinitionId(id).uniqueResult();
		return df;
	}

	/* Setters / Getters for Spring injection */
	
	/**
	 * @return the processKey
	 */
	public String getProcessKey() {
		return processKey;
	}

	/**
	 * @param processKey the processKey to set
	 */
	public void setProcessKey(String processKey) {
		this.processKey = processKey;
	}
	
}
